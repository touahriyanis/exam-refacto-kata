﻿using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using SalesReporterKata.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Factories
{
    public static class ReportingFactory
    {
        public static IReportingStrategy GetStrategyByReadType(ReadType readType, ILogger logger) => readType switch
        {
            ReadType.print => new PrintingStrategy(logger),
            ReadType.report => new ReportingStrategy(logger),
            _ => new ReportingErrorStrategy(logger),
        };
    }
}
