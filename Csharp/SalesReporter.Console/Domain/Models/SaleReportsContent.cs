﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Models
{
    public class SaleReportsContent
    {
        public SaleReportsContent(IEnumerable<SaleReport> saleReports, string[] columnNames)
        {
            SaleReports = saleReports;
            ColumnNames = columnNames;
        }

        public IEnumerable<SaleReport> SaleReports { get; set; }

        public string[] ColumnNames { get; set; }
    }
}
