﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Models
{
    public enum KpiType
    {
        SalesTotal,
        ClientTotal
    }
}
