﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Models
{
    public class SaleReport
    {
        public SaleReport(int orderId, string userName, int numberOfItems, double totalOfBasket, string dateOfBuy)
        {
            OrderId = orderId;
            UserName = userName;
            NumberOfItems = numberOfItems;
            TotalOfBasket = totalOfBasket;
            DateOfBuy = dateOfBuy;
        }

        public int OrderId { get; set; }
        public string UserName { get; set; }
        public int NumberOfItems { get; set; }
        public double TotalOfBasket { get; set; }
        public string DateOfBuy { get; set; }
    }
}
