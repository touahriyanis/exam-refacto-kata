﻿using SalesReporterKata.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Interfaces
{
    public interface IKpiCalculationStrategy
    {
        int CalculateKpi(SaleReport report);
    }
}
