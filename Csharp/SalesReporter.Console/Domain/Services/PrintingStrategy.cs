﻿using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Services
{
    public class PrintingStrategy : IReportingStrategy
    {
        public ILogger logger;

        public PrintingStrategy(ILogger logger)
        {
            this.logger = logger;
        }

        public void Report(SaleReportsContent salesReport)
        {
			logger.Log("=== Sales Viewer ===");

			var headerString = string.Join(
					" | ",
					salesReport.ColumnNames.Select(
						(val, ind) => val.PadLeft(16)));

			logger.Log("+" + new string('-', headerString.Length + 2) + "+");
			logger.Log("| " + headerString + " |");
			logger.Log("+" + new string('-', headerString.Length + 2) + "+");

			foreach (SaleReport saleReport in salesReport.SaleReports)
			{
				var tableLine = string.Join(
				   " | ",

				   saleReport.GetType().GetProperties().Select(
					   (prop) => prop?.GetValue(saleReport, null)?.ToString()?.PadLeft(16)));

				logger.Log($"| {tableLine} |");
			}

			logger.Log("+" + new string('-', headerString.Length + 2) + "+");
		}
    }
}
