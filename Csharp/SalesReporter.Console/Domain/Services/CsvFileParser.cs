﻿using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Services
{
    public class CsvFileParser : IParser
    {
        public SaleReportsContent ParseFile(string fileName)
        {
            string[] salesRowsString = File.ReadAllLines(fileName);

			List<SaleReport> saleReports = new List<SaleReport>();
			string[] columns = salesRowsString[0].Split(',');

			var salesRowsStringWithoutHeader = salesRowsString.Skip(1);

			foreach (string sale in salesRowsStringWithoutHeader)
			{
				var saleData = sale.Split(',');

				saleReports.Add(new SaleReport(int.Parse(saleData[0]), saleData[1].Trim(), int.Parse(saleData[2]), double.Parse(saleData[3], CultureInfo.InvariantCulture), saleData[4].Trim()));
			}

			return new SaleReportsContent(saleReports, columns);
		}
    }
}
