﻿using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Services
{
    public class ReportingStrategy : IReportingStrategy
    {
		public ILogger logger;

		public ReportingStrategy(ILogger logger)
		{
			this.logger = logger;
		}

		public void Report(SaleReportsContent salesReport)
        {
			logger.Log("=== Sales Viewer ===");

			// TODO : Créer des stratégies differnetes pour le calcul des differents KPI ici
			// avec des tests unitaires pour verifier chaque cas

			int totalNumberOfSales = salesReport.SaleReports.Count();

			int numberOfClients = 0;

			int numberOfItemsSold = salesReport.SaleReports.Sum(a => a.NumberOfItems);

			double totalSalesAmount = salesReport.SaleReports.Sum(a => a.TotalOfBasket);

			double averageAmountSale = Math.Round(totalSalesAmount / totalNumberOfSales, 2);
			double averageItemPrice = Math.Round(totalSalesAmount / numberOfItemsSold, 2);

			logger.Log($"+{new String('-', 45)}+");
			logger.Log($"| {" Number of sales".PadLeft(30)} | {totalNumberOfSales.ToString().PadLeft(10)} |");
			logger.Log($"| {" Number of clients".PadLeft(30)} | {numberOfClients.ToString().PadLeft(10)} |");
			logger.Log($"| {" Total items sold".PadLeft(30)} | {numberOfItemsSold.ToString().PadLeft(10)} |");
			logger.Log($"| {" Total sales amount".PadLeft(30)} | {Math.Round(totalSalesAmount, 2).ToString().PadLeft(10)} |");
			logger.Log($"| {" Average amount/sale".PadLeft(30)} | {averageAmountSale.ToString().PadLeft(10)} |");
			logger.Log($"| {" Average item price".PadLeft(30)} | {averageItemPrice.ToString().PadLeft(10)} |");
			logger.Log($"+{new String('-', 45)}+");
		}
    }
}
