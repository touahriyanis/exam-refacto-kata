﻿using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesReporterKata.Domain.Services
{
    public class ReportingErrorStrategy : IReportingStrategy
    {
        public ILogger logger;

        public ReportingErrorStrategy(ILogger logger)
        {
            this.logger = logger;
        }

        public void Report(SaleReportsContent salesReport)
        {
            logger.Log("[ERR] your command is not valid ");
            logger.Log("Help: ");
            logger.Log("    - [print]  : show the content of our commerce records in data.csv");
            logger.Log("    - [report] : show a summary from data.csv records ");
        }
    }
}
