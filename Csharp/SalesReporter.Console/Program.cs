﻿using SalesReporterKata.Domain.Factories;
using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using SalesReporterKata.Domain.Services;
using System.Globalization;

public static class Program
{
	public static void Main(string[] args)
	{
		// execution with dependency injection
		Execute(args, new CsvFileParser(), new ReporterLogger());
	}

	public static void Execute(string[] args, IParser fileParser, ILogger logger)
    {
		bool succeded = Enum.TryParse(args[0], out ReadType readType);
		if(!succeded)
        {
			readType = ReadType.error;
		}

		IReportingStrategy reportingStrategy = ReportingFactory.GetStrategyByReadType(readType, logger);

		string filename = args[1];
		SaleReportsContent salesReport = fileParser.ParseFile(filename);

		reportingStrategy.Report(salesReport);
	}
}
