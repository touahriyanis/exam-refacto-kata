﻿using Newtonsoft.Json;
using NFluent;
using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using SalesReporterKata.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SalesReporter.Cli.Tests
{
    public class FileParserTests
    {
        [Fact]
        public void EmptySampleData_on_file_parsing()
        {
            string[] expectedColumnNames = { "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy" };
            List<SaleReport> expectedSaleReports = new List<SaleReport>();

            IParser fileParser = new CsvFileParser();
            SaleReportsContent result = fileParser.ParseFile("./empty_data.csv");

            var serializedSaleReports = JsonConvert.SerializeObject(result.SaleReports);
            var serializedExpectedSaleReports = JsonConvert.SerializeObject(expectedSaleReports);

            Assert.Equal(serializedExpectedSaleReports, serializedSaleReports);
            Assert.Equal(expectedColumnNames, result.ColumnNames);
        }

        [Fact]
        public void SampleData_on_file_parsing()
        {
            string[] expectedColumnNames = { "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy" };
            List<SaleReport> expectedSaleReports = new List<SaleReport>();
            expectedSaleReports.Add(new SaleReport(1, "peter", 3, 123, "2021-11-30"));
            expectedSaleReports.Add(new SaleReport(2, "paul", 1, 433.5, "2021-12-11"));
            expectedSaleReports.Add(new SaleReport(3, "peter", 1, 329.99, "2021-12-18"));
            expectedSaleReports.Add(new SaleReport(4, "john", 5, 467.35, "2021-12-30"));
            expectedSaleReports.Add(new SaleReport(5, "john", 1, 88, "2022-01-04"));

            IParser fileParser = new CsvFileParser();
            SaleReportsContent result = fileParser.ParseFile("./data.csv");

            var serializedSaleReports = JsonConvert.SerializeObject(result.SaleReports);
            var serializedExpectedSaleReports = JsonConvert.SerializeObject(expectedSaleReports);

            Assert.Equal(serializedExpectedSaleReports, serializedSaleReports);
            Assert.Equal(expectedColumnNames, result.ColumnNames);
        }
    }
}
