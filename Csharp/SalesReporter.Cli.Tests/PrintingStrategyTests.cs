﻿using NFluent;
using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Models;
using SalesReporterKata.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SalesReporter.Cli.Tests
{
    public class PrintingStrategyTests
    {
        [Fact]
        public void SampleData_on_print_strategy()
        {
            using var writer = new StringWriter();
            Console.SetOut(writer);
            Console.SetError(writer);

            string[] expectedColumnNames = { "orderid", "userName", "numberOfItems", "totalOfBasket", "dateOfBuy" };
            List<SaleReport> expectedSaleReports = new List<SaleReport>();
            expectedSaleReports.Add(new SaleReport(1, "peter", 3, 123, "2021-11-30"));
            expectedSaleReports.Add(new SaleReport(2, "paul", 1, 433.5, "2021-12-11"));
            expectedSaleReports.Add(new SaleReport(3, "peter", 1, 329.99, "2021-12-18"));
            expectedSaleReports.Add(new SaleReport(4, "john", 5, 467.35, "2021-12-30"));
            expectedSaleReports.Add(new SaleReport(5, "john", 1, 88, "2022-01-04"));

            SaleReportsContent salesReportContent = new SaleReportsContent(expectedSaleReports, expectedColumnNames);

            IReportingStrategy reporter = new PrintingStrategy(new ReporterLogger());
            reporter.Report(salesReportContent);

            var sut = writer.ToString();
            Assert.Equal(sut,
        @$"=== Sales Viewer ===
+----------------------------------------------------------------------------------------------+
|          orderid |         userName |    numberOfItems |    totalOfBasket |        dateOfBuy |
+----------------------------------------------------------------------------------------------+
|                1 |            peter |                3 |              123 |       2021-11-30 |
|                2 |             paul |                1 |            433,5 |       2021-12-11 |
|                3 |            peter |                1 |           329,99 |       2021-12-18 |
|                4 |             john |                5 |           467,35 |       2021-12-30 |
|                5 |             john |                1 |               88 |       2022-01-04 |
+----------------------------------------------------------------------------------------------+
");
        }

    }
}
