﻿using NFluent;
using SalesReporterKata.Domain.Interfaces;
using SalesReporterKata.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SalesReporter.Cli.Tests;

public class LoggerTests
{
    public ILogger logger;

    public LoggerTests()
    {
        this.logger = new ReporterLogger();
    }

    [Fact]
    public void Console_writeline_on_log()
    {
        using var logWriter = new StringWriter();
        Console.SetOut(logWriter);
        Console.SetError(logWriter);

        this.logger.Log("Hello World!");

        var sut = logWriter.ToString();
        Assert.Equal(@"Hello World!
", sut);
    }
}
