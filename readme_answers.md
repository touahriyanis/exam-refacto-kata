# Points négatifs 
- Code illisible, nom de variables non explicites
- Trop de commentaires, des fois qui ne refletent pas le code présenté.
- Maintenance donc, compliqué
- Toute la logique métier de l'application est faite dans une suite d'instructions dans un seul fichier. le domaine et l'infrastructure sont mélangés
ensemble (Parsing du fichier csv, les calculs, le logging...)

# Comment on l'aurait fait
- La structure doit entierement changer. Nous devons séparer les enitités métier et infrastructure.

Enitités métier :
 - Printing (Affichage des données csv dans le format voulu)
 - Reporting (Calculs des KPIs et Affichage du Rapport)
Entités Infra :
 - Parser de fichier csv
 - Logger
 - Gestion de Dates

 * Pour le printing, on auran une interface et une implementation qui s'occupe de générer l'affichage voulu
 * Pour le reporting, on serait parti sur un strategy pattern, qui selong le type de KPI (Nombre total de clients, Nombre total des ventes... etc) va nous faire le calcul necessaire à partir des données générées pas le parser du fichier csv.
 * On serait parti sur un factory pattern pour lancer le processus de printing ou de reporting selon la commande de l'utilisateur.

# Stratégie de tests
Partir sur du TDD.
Voici les points importants à tester :
 - à partir d'un fichier csv
 * Verifier que le modéle genéré à partir du Parser sont égales aux données du fichier. Il est important d'avoir des données cohérentes.
 * Verifier que le format d'affichage est bien respecté pour notre module Printing, car c'est un besoin metier important.
 * Verifier que chaque KPI est bien calculé (c'est également un besoin metier important) :
    - Number of sales
    - Number of clients
    - Total items sold
    - Total sales amount
    - Average amount/sale
    - Average item price
